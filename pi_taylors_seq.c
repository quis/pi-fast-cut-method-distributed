#include <stdio.h>
#include <math.h>

#define ITERATIONS_COUNT 10000

int main(int argc, char *argv[]) {
	int i;
	long double result[2];
	long double finalResult;
	result[0] = 0.0;
	result[1] = 0.0;
	
	for(i=0; i<ITERATIONS_COUNT; i++) {
		result[0] += pow(-1,i)/(((2*i)+1)*pow(5,(2*i)+1));
	}
	for(i=0; i<ITERATIONS_COUNT; i++) {
		result[1] += pow(-1,i)/(((2*i)+1)*pow(239,(2*i)+1));
	}
	finalResult = (result[0]*16)-(result[1]*4);
	printf("%.20Lf\n", finalResult);
	return 0;
}
