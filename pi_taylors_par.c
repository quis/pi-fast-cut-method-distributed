#include <stdio.h>
#include <math.h>
#include <mpi.h>

#define ROOT 0
#define RESULT_TAG 100

#define ITERATIONS_COUNT 10000

int main(int argc, char *argv[]) {
	int threadsCount, threadId;

	MPI_Init(&argc, &argv); 

    MPI_Comm_size( MPI_COMM_WORLD, &threadsCount );
    MPI_Comm_rank( MPI_COMM_WORLD, &threadId );

	if(threadId == ROOT) {
		int i = 0;
		long double threadResult[2];
		long double result[2];
		long double finalResult;
		MPI_Status status;
		result[0] = 0.0;
		result[1] = 0.0;
	
		for (i=1;i<threadsCount;i++) {
			MPI_Recv( &threadResult, 2, MPI_LONG_DOUBLE, i, RESULT_TAG, MPI_COMM_WORLD, &status);
			result[0] += threadResult[0];
			result[1] += threadResult[1];
		}
		
		finalResult = ((result[0]*16)-(result[1]*4));
		printf("Uzyskana wartosc PI:\n%.20Lf\n", finalResult);
	} else {
		int i = 0;
		long double threadResult[2];
		threadResult[0] = 0.0;
		threadResult[1] = 0.0;
		for(i=(threadId-1); i<ITERATIONS_COUNT; i+=threadsCount) {
			threadResult[0] += pow(-1,i)/(((2*i)+1)*pow(5,(2*i)+1));
		}
		for(i=(threadId-1); i<ITERATIONS_COUNT; i+=threadsCount) {
			threadResult[1] += pow(-1,i)/(((2*i)+1)*pow(239,(2*i)+1));
		}
		MPI_Send( &threadResult, 2, MPI_LONG_DOUBLE, ROOT, RESULT_TAG, MPI_COMM_WORLD );
	}
	
	 MPI_Finalize();
	return 0;
}
